﻿using UnityEngine;
using System.Collections;

public class SpotlightDetectBackup : MonoBehaviour {
    public Transform target;
    public Vector2 distance;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void detect(float spotx, float spoty)
    {
        PlayerMovement player = (PlayerMovement)FindObjectOfType(typeof(PlayerMovement));
        CircleCollider2D collider = gameObject.GetComponent<CircleCollider2D>();
        Vector2 center = new Vector2(spotx, spoty);
        target = player.transform;
        Vector2 distance = (Vector2)target.position - center;
        if (distance.magnitude <= collider.radius)
        {
            SpotDarkManager.spotlight = true;
        } else
        {
            SpotDarkManager.spotlight = false;
        }
    }


}
